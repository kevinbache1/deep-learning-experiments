import os
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM, Embedding
from keras.callbacks import ModelCheckpoint, LambdaCallback
from keras.utils import np_utils, to_categorical
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

import glob, os


def load_raw_data(path):
    base_dir = path

    raw_text = ""
    for file in glob.glob(os.path.join(base_dir, "*.txt")):
        raw_text_one_file = open(file, "rb").read().decode('utf8').lower()
        raw_text = raw_text + '\n' + raw_text_one_file
    return raw_text


def get_sentences(raw_text):
    input_text = raw_text
    input_text = input_text.replace('\n', '.')
    sentences = input_text.split('.')
    sentences = [sentence.strip() for sentence in sentences if sentence and sentence.strip() != '{sentenceend}']
    return sentences


def tokenize_and_encode(sentences, vocab_size, seq_length):
    tokenizer = Tokenizer(num_words=vocab_size, filters='!"#$%&()*+,-./:;<=>?@[\]^_`|~ ')
    tokenizer.fit_on_texts(sentences)
    encoded_docs = tokenizer.texts_to_sequences(sentences)
    X_seq = list()
    y_next = list()
    for doc in encoded_docs:
        for i, word in enumerate(doc):
            x_tmp = []
            if i == 0:
                continue
            if i <= seq_length:
                x_tmp = doc[0:i]
            else:
                x_tmp = doc[i - 5:i]
            if i < len(doc) - 1:
                y_tmp = doc[i + 1]
            else:
                continue
            X_seq.append(x_tmp)
            y_next.append(y_tmp)

    X_seq = np.array(X_seq)
    y_next = np.array(y_next)
    X = pad_sequences(X_seq, value=-1)

    y = to_categorical(y_next)

    return X, y, tokenizer


def get_model(seq_length, vocab_size):
    model = Sequential()
    # model.add(Embedding(vocab_size, 50, input_length=seq_length))
    model.add(LSTM(100, return_sequences=True, input_shape=(seq_length, 1)))
    model.add(LSTM(100))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(vocab_size, activation='softmax'))
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    print(model.summary())
    return model


if __name__ == '__main__':
    vocab_size = 600
    seq_length = 5
    epochs = 250
    batch_size = 1024
    raw_text = load_raw_data("/root/project/deep-learning-experiments/data")
    sentences = get_sentences(raw_text)
    X, y, tokenizer = tokenize_and_encode(sentences, vocab_size, seq_length)

    model = get_model(seq_length, vocab_size)

    X = X.reshape(X.shape[0], X.shape[1], 1)
    model.fit(X, y, epochs=epochs, batch_size=batch_size)
    model.save('/root/project/deep-learning-experiments/model_weights.hd5')